﻿namespace DTCCFileViewer
{
    using System;
    using System.Text.RegularExpressions;

    public class Utils
    {
        public static string RemoveDigitsFromStart(string str)
        {
            char[] trimChars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            return str.TrimStart(trimChars);
        }

        public static string RemoveSpecialCharacters(string str) => 
            Regex.Replace(str, "[^a-zA-Z0-9_]+", "", RegexOptions.Compiled);

        public static string RemoveSpecialCharactersForXMLElementNames(string str) => 
            RemoveDigitsFromStart(RemoveSpecialCharacters(str));
    }
}

