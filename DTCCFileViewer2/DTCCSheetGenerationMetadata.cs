﻿namespace DTCCFileViewer
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DTCCSheetGenerationMetadata
    {
        public List<KeyValuePair<string, int>> RelevantColumns = new List<KeyValuePair<string, int>>();

        public int EndsAtLine { get; set; }

        public string ParentId { get; set; }

        public string SheetName { get; set; }

        public int StartsAtLine { get; set; }

        public int TxtFileUniqueIdLength { get; set; }

        public int TxtFileUniqueIdStartsAt { get; set; }

        public string UniqueId { get; set; }
    }
}

