﻿namespace DTCCFileViewer
{
    using System;
    using System.Runtime.CompilerServices;

    public class DTCCLineItemMetadata
    {
        public string Description { get; set; }

        public string End { get; set; }

        public string FieldName { get; set; }

        public string ItemNumber { get; set; }

        public string Length { get; set; }

        public string RejectCode { get; set; }

        public string RequiredIndicator { get; set; }

        public string Start { get; set; }

        public string Type { get; set; }
    }
}

