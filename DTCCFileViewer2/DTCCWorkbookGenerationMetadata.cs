﻿namespace DTCCFileViewer
{
    using System;
    using System.Collections.Generic;

    public class DTCCWorkbookGenerationMetadata
    {
        public List<DTCCSheetGenerationMetadata> DTCCSheets = new List<DTCCSheetGenerationMetadata>();
    }
}

