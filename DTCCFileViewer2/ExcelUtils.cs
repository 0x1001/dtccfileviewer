﻿namespace DTCCFileViewer
{
    using NPOI.SS.UserModel;
    using System;

    public class ExcelUtils
    {
        public static string GetCellValue(ISheet sheet, int row, int column)
        {
            string str = string.Empty;
            if (sheet.GetRow(row).GetCell(column) != null)
            {
                switch (sheet.GetRow(row).GetCell(column).CellType)
                {
                    case CellType.Numeric:
                        return Convert.ToString(sheet.GetRow(row).GetCell(column).NumericCellValue);

                    case CellType.String:
                        return sheet.GetRow(row).GetCell(column).StringCellValue;

                    case CellType.Formula:
                        return sheet.GetRow(row).GetCell(column).CellFormula;

                    case CellType.Blank:
                        return str;

                    case CellType.Boolean:
                        return Convert.ToString(sheet.GetRow(row).GetCell(column).BooleanCellValue);

                    case CellType.Error:
                        return Convert.ToString(sheet.GetRow(row).GetCell(column).ErrorCellValue);
                }
            }
            return str;
        }
    }
}

