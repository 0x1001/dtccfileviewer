﻿using System.Windows;
using System.IO;
using Microsoft.Win32;
using ICSharpCode.AvalonEdit.Highlighting;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Controls;
using ICSharpCode.AvalonEdit.Document;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;
using ICSharpCode.AvalonEdit.Rendering;
using System.Windows.Media;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using ICSharpCode.AvalonEdit.Sample;

namespace DTCCFileViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            AppDomain.CurrentDomain.UnhandledException += DTCCFileViewerUnhandledExceptionHandler;
            InitializeComponent();
            DTCCUtils.InitializeSupportedFormats();

            // Enable AvalonEdit Text Search
            ICSharpCode.AvalonEdit.Search.SearchPanel.Install(textEditor);
        }

        private void DTCCFileViewerUnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Unexpected error occurred! Closing application. Exception: " + e, "Unexpected Exception Occurred", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        string currentFileName = string.Empty;

        private void OpenFileClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.CheckFileExists = true;

            if (dlg.ShowDialog() ?? false)
            {
                currentFileName = dlg.FileName;
                UpdateViews();
            }
        }

        private void UpdateViews()
        {
            //DtccView.Items.Clear();

            #region Optimization recommended for opening large files

            textEditor.Document = null;
            //          xmlEditor.Document = null;

            TextDocument txtDocument = new TextDocument();
            int prevUndoSizeLimit = txtDocument.UndoStack.SizeLimit;
            txtDocument.UndoStack.SizeLimit = 0;

            using (FileStream fs = new FileStream(currentFileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    txtDocument.Text = reader.ReadToEnd();
                }
            }

            textEditor.Document = txtDocument;
            txtDocument.UndoStack.SizeLimit = prevUndoSizeLimit;
            textEditor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinitionByExtension(Path.GetExtension(currentFileName));            

            #endregion


            //XElement treeXml = DTCCUtils.BuildDTCCXml(textEditor.Document);
            XElement treeXml = DTCCUtils.BuildDTCCXml(currentFileName);

            XDocument computedXML = new XDocument(treeXml);
            //computedXML.Save(@"C:\Users\aviswanathan\SkyDrive Pro\RSL\DTCC_RSL_SAMPLE_OUTPUT\DeathClaim-DTCC\MegaDeathClaim.xml");
            Stream stream = new MemoryStream();  // Create a stream
            computedXML.Save(stream);      // Save XDocument into the stream
            stream.Position = 0;   // Rewind the stream ready to read from it elsewhere
                                   //            TextDocument xmlDocument = new TextDocument(); 
            
            xmlEditor.Load(stream);
            stream.Close();
            //UpdateFolding(xmlEditor);

            //Use the XDP that has been created as one of the Window's resources ...
            XmlDataProvider dp = (XmlDataProvider)this.FindResource("xmlDP");
            //... and assign the XDoc to it, using the XDoc's root.
            var xmldoc = new XmlDocument();
            xmldoc.Load(computedXML.CreateReader());
            dp.Document = xmldoc;
            dp.XPath = "*";

            //BuildTree(DtccView, new XDocument(treeXml));
            //DtccView.Visibility = Visibility.Visible;
        }

        private void SaveFileClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(currentFileName))
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.DefaultExt = ".txt";
                if (dlg.ShowDialog() ?? false)
                {
                    currentFileName = dlg.FileName;
                    UpdateViews();
                }
                else
                {
                    return;
                }
            }
            textEditor.Save(currentFileName);
        }

        private void ReloadFileClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(currentFileName))
            {
                textEditor.Load(currentFileName);
            }
        }


        #region Folding
        FoldingManager foldingManager;
        object foldingStrategy;

        void UpdateFolding(TextEditor textEditor)
        {
            if (textEditor.SyntaxHighlighting == null)
            {
                foldingStrategy = null;
            }
            else
            {
                switch (textEditor.SyntaxHighlighting.Name)
                {
                    case "XML":
                        foldingStrategy = new XmlFoldingStrategy();
                        textEditor.TextArea.IndentationStrategy = new ICSharpCode.AvalonEdit.Indentation.DefaultIndentationStrategy();
                        break;
                    case "C#":
                    case "C++":
                    case "PHP":
                    case "Java":
                        textEditor.TextArea.IndentationStrategy = new ICSharpCode.AvalonEdit.Indentation.CSharp.CSharpIndentationStrategy(textEditor.Options);
                        foldingStrategy = new BraceFoldingStrategy();
                        break;
                    default:
                        textEditor.TextArea.IndentationStrategy = new ICSharpCode.AvalonEdit.Indentation.DefaultIndentationStrategy();
                        foldingStrategy = null;
                        break;
                }
            }
            if (foldingStrategy != null)
            {
                if (foldingManager == null)
                    foldingManager = FoldingManager.Install(textEditor.TextArea);
                UpdateFoldings(textEditor);
            }
            else
            {
                if (foldingManager != null)
                {
                    FoldingManager.Uninstall(foldingManager);
                    foldingManager = null;
                }
            }
        }

        void UpdateFoldings(TextEditor textEditor)
        {
            if (foldingStrategy is BraceFoldingStrategy)
            {
                ((BraceFoldingStrategy)foldingStrategy).UpdateFoldings(foldingManager, textEditor.Document);
            }
            if (foldingStrategy is XmlFoldingStrategy)
            {
                ((XmlFoldingStrategy)foldingStrategy).UpdateFoldings(foldingManager, textEditor.Document);
            }
        }
        #endregion

        private void BuildTree(TreeView treeView, XDocument doc)
        {
            TreeViewItem treeNode = new TreeViewItem
            {
                //Should be Root
                Header = doc.Root.Name.LocalName,
                IsExpanded = false
            };
            treeView.Items.Add(treeNode);
            BuildNodes(treeNode, doc.Root);
        }

        private void BuildNodes(TreeViewItem treeNode, XElement element)
        {
            foreach (XNode child in element.Nodes())
            {
                switch (child.NodeType)
                {
                    case XmlNodeType.Element:
                        XElement childElement = child as XElement;
                        var collection = childElement.Attributes();
                        TreeViewItem childTreeNode = new TreeViewItem
                        {
                            //Get First attribute where it is equal to value
                            Header = childElement.Name, // + " = " + childElement.Value.Trim(),
                            Tag = collection,
                            //Automatically expand elements
                            IsExpanded = false
                        };
                        treeNode.Items.Add(childTreeNode);
                        BuildNodes(childTreeNode, childElement);
                        break;
                        //case XmlNodeType.Text:
                        //    XText childText = child as XText;
                        //    treeNode.Items.Add(new TreeViewItem { Header = childText.Value, });
                        //    break;
                }
            }
        }


        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            XmlElement selected = DtccView.SelectedItem as XmlElement;
            if (selected != null)
            {
                int lineNumber = 1;
                int start = -1;
                int length = -1;

                // The condition should take care of a new file being opened and the tree being cleared.
                if (DtccView.SelectedItem != null)
                {
                    XmlAttributeCollection attributes = selected.Attributes;

                    DocumentLine line = textEditor.Document.GetLineByOffset(textEditor.CaretOffset);
                    lineNumber = line.LineNumber;

                    if (attributes != null)
                    {
                        foreach (XmlAttribute xa in attributes)
                        {
                            if (xa != null)
                            {
                                if (xa.Name == "LineNumber")
                                {
                                    lineNumber = Int32.Parse(xa.Value);
                                }

                                if (xa.Name == "Start")
                                {
                                    start = Int32.Parse(xa.Value);
                                }

                                if (xa.Name == "Length")
                                {
                                    length = Int32.Parse(xa.Value);
                                }
                            }
                        }
                    }

                    textEditor.ScrollToLine(lineNumber);
                }

                //DocumentLine line = textEditor.Document.GetLineByOffset(textEditor.CaretOffset);
                //textEditor.Select(line.Offset, line.Length);
                //DocumentLine line = textEditor.Document.GetLineByNumber(lineNumber);



                //DocumentLine editorLine = textEditor.Document.GetLineByOffset(textEditor.CaretOffset);
                //DocumentLine editorLine = textEditor.Document.GetLineByNumber(lineNumber);
                if (start != -1 && length != -1)
                {
                    //DocumentLine editorLine = textEditor.Document.GetLineByNumber(lineNumber);
                    //textEditor.Select(editorLine.Offset, editorLine.Length);
                    //textEditor.Select(start, length);
                    //textEditor.CaretOffset = lineNumber;
                    //textEditor.SelectionStart = start;
                    //textEditor.SelectionLength = length;


                    //OffsetColorizer offsetColorizer = new OffsetColorizer();
                    //offsetColorizer.StartOffset = start;
                    //offsetColorizer.EndOffset = start + length;
                    //textEditor.TextArea.TextView.LineTransformers.Add(offsetColorizer);
                    //textEditor.TextArea.TextView.Redraw();

                    textEditor.TextArea.TextView.LineTransformers.Clear();
                    textEditor.TextArea.TextView.LineTransformers.Add(new LineColorizer(lineNumber, start, length));
                    textEditor.TextArea.Caret.Line = lineNumber;

                    DocumentLine editorLine = textEditor.Document.GetLineByNumber(lineNumber);
                    textEditor.TextArea.Caret.Offset = editorLine.Offset + start - 1;
                    textEditor.TextArea.Caret.BringCaretToView();
                    textEditor.TextArea.Caret.Show();

                }
                else
                {
                    textEditor.TextArea.TextView.LineTransformers.Clear();
                    DocumentLine editorLine = textEditor.Document.GetLineByNumber(lineNumber);
                    textEditor.TextArea.TextView.LineTransformers.Add(new LineColorizer(lineNumber, 1, editorLine.Length));
                }
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //BackgroundWorker worker = new BackgroundWorker();
            //worker.WorkerReportsProgress = true;
            //worker.DoWork += Worker_DoWork;
            //worker.ProgressChanged += Worker_ProgressChanged;

            //worker.RunWorkerAsync();

        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //loadingStatusProgressBar.Value = e.ProgressPercentage;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(100);
            }
        }
    }

    //TODO: Incorporate the offset logic in the other Line Colorizer and DELETE this.
    public class OffsetColorizer : DocumentColorizingTransformer
    {
        public int StartOffset { get; set; }
        public int EndOffset { get; set; }

        protected override void ColorizeLine(DocumentLine line)
        {
            if (line.Length == 0)
                return;

            if (line.Offset < StartOffset || line.Offset > EndOffset)
                return;

            int start = line.Offset > StartOffset ? line.Offset : StartOffset;
            int end = EndOffset > line.EndOffset ? line.EndOffset : EndOffset;

            ChangeLinePart(start, end, element => element.TextRunProperties.SetForegroundBrush(Brushes.Red));
        }

        void ApplyChanges(VisualLineElement element)
        {
            // apply changes here
            element.TextRunProperties.SetForegroundBrush(Brushes.Red);
        }
    }

    //TODO: DELETE
    /*
    public class Conference
    {
        public Conference(string name)
        {
            Name = name;
            Teams = new ObservableCollection<Team>();
        }

        public string Name { get; private set; }
        public ObservableCollection<Team> Teams { get; private set; }
    }

    public class Team
    {
        public Team(string name)
        {
            Name = name;
            Players = new ObservableCollection<string>();
        }

        public string Name { get; private set; }
        public ObservableCollection<string> Players { get; private set; }
    }

    public void InitTestHierarchy()
      {
          var western = new Conference("Western")
          {
              Teams =
              {
                  new Team("Club Deportivo Chivas USA"),
                  new Team("Colorado Rapids"),
                  new Team("FC Dallas"),
                  new Team("Houston Dynamo"),
                  new Team("Los Angeles Galaxy"),
                  new Team("Real Salt Lake"),
                  new Team("San Jose Earthquakes"),
                  new Team("Seattle Sounders FC") { Players = { "Osvaldo Alonso", "Evan Brown" }},
                  new Team("Portland 2011"),
                  new Team("Vancouver 2011")
              }
          };

          var eastern = new Conference("Eastern")
          {
              Teams =
              {
                  new Team("Chicago Fire"),
                  new Team("Columbus Crew"),
                  new Team("D.C. United"),
                  new Team("Kansas City Wizards"),
                  new Team("New York Red Bulls"),
                  new Team("New England Revolution"),
                  new Team("Toronto FC"),
                  new Team("Philadelphia Union 2010")
              }
          };

          var league = new Collection<Conference>() { western, eastern };


          DataContext = new
          {
              WesternConference = western,
              EasternConference = eastern,
              League = league
          };
      }

  */


}
