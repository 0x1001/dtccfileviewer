﻿namespace DTCCFileViewer
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DTCCSheetMetadata
    {
        public List<DTCCLineItemMetadata> DTCCLineItemMetadatas = new List<DTCCLineItemMetadata>();

        public string ParentId { get; set; }

        public string SheetName { get; set; }

        public int TxtFileUniqueIdLength { get; set; }

        public int TxtFileUniqueIdStartsAt { get; set; }

        public string UniqueId { get; set; }
    }
}

