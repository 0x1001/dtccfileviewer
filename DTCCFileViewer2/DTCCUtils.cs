﻿namespace DTCCFileViewer
{
    using NPOI.HSSF.UserModel;
    using NPOI.SS.UserModel;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    public class DTCCUtils
    {
        private static Dictionary<string, DTCCFormatMetadata> supportedDTCCFormats = new Dictionary<string, DTCCFormatMetadata>();

        #region BUILD DATATRAK WORKBOOK TOPLEVEL METADATA
        public static DTCCWorkbookGenerationMetadata BuildDATATRAKTopLevelMetadata()
        {
            DTCCWorkbookGenerationMetadata dtccWorkbookGenMetaData = new DTCCWorkbookGenerationMetadata();
            // Relevant Columns
            List<KeyValuePair<String, int>> RelevantColumns = new List<KeyValuePair<String, int>>();
            RelevantColumns.Add(new KeyValuePair<string, int>("Start", 1));
            RelevantColumns.Add(new KeyValuePair<string, int>("End", 2));
            RelevantColumns.Add(new KeyValuePair<string, int>("Length", 3));
            RelevantColumns.Add(new KeyValuePair<string, int>("Type", 4));
            RelevantColumns.Add(new KeyValuePair<string, int>("FieldName", 5));
            RelevantColumns.Add(new KeyValuePair<string, int>("Description", 6));
            RelevantColumns.Add(new KeyValuePair<string, int>("ItemNumber", 7));
            RelevantColumns.Add(new KeyValuePair<string, int>("RequiredIndicator", 8));
            RelevantColumns.Add(new KeyValuePair<string, int>("RejectCode", 9));

            // Datatrak Header
            DTCCSheetGenerationMetadata dtccSheetGenMetaData = new DTCCSheetGenerationMetadata();
            dtccSheetGenMetaData.SheetName = "DATATRAK HEADER";
            dtccSheetGenMetaData.StartsAtLine = 7;
            dtccSheetGenMetaData.EndsAtLine = 34;
            dtccSheetGenMetaData.TxtFileUniqueIdStartsAt = 1;
            dtccSheetGenMetaData.TxtFileUniqueIdLength = 2;
            dtccSheetGenMetaData.UniqueId = "HDR";
            dtccSheetGenMetaData.ParentId = String.Empty;
            dtccSheetGenMetaData.RelevantColumns = RelevantColumns;
            dtccWorkbookGenMetaData.DTCCSheets.Add(dtccSheetGenMetaData);

            // Datatrak END
            dtccSheetGenMetaData = new DTCCSheetGenerationMetadata();
            dtccSheetGenMetaData.SheetName = "DATATRAK END RECORD ";
            dtccSheetGenMetaData.StartsAtLine = 7;
            dtccSheetGenMetaData.EndsAtLine = 21;
            dtccSheetGenMetaData.TxtFileUniqueIdStartsAt = 1;
            dtccSheetGenMetaData.TxtFileUniqueIdLength = 2;
            dtccSheetGenMetaData.UniqueId = "END";
            dtccSheetGenMetaData.ParentId = "HDR";
            dtccSheetGenMetaData.RelevantColumns = RelevantColumns;
            dtccWorkbookGenMetaData.DTCCSheets.Add(dtccSheetGenMetaData);

            return dtccWorkbookGenMetaData;
        }
        #endregion


        public static DTCCWorkbookGenerationMetadata BuildAPPTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 5),
                new KeyValuePair<string, int>("Description", 6),
                new KeyValuePair<string, int>("ItemNumber", 7),
                new KeyValuePair<string, int>("RequiredIndicator", 8),
                new KeyValuePair<string, int>("RejectCode", 9)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "30",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Header",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "32",
                ParentId = "30",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application-Sub-Pay Record",
                StartsAtLine = 7,
                EndsAtLine = 0x2c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3301",
                ParentId = "32",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application Date Record",
                StartsAtLine = 7,
                EndsAtLine = 70,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3302",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application Contract Record ",
                StartsAtLine = 7,
                EndsAtLine = 0x17,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3303",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Method",
                StartsAtLine = 6,
                EndsAtLine = 0x1b,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3304",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Type Record",
                StartsAtLine = 6,
                EndsAtLine = 0x10,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3306",
                ParentId = "3304",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Cost Basis Record",
                StartsAtLine = 6,
                EndsAtLine = 13,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3307",
                ParentId = "3306",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Funds Record",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3310",
                ParentId = "3306",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Qualified Transfer Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1b,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3313",
                ParentId = "3306",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Qual Trans Prior Carr Add",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3314",
                ParentId = "3313",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Transfer Prior Con Entity",
                StartsAtLine = 7,
                EndsAtLine = 0x1c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3315",
                ParentId = "3313",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Type Remarks Record",
                StartsAtLine = 6,
                EndsAtLine = 12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3316",
                ParentId = "3306",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Entity ID Record",
                StartsAtLine = 6,
                EndsAtLine = 0x24,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3317",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Entity Relationship Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1a,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3318",
                ParentId = "3317",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Entity Address Record",
                StartsAtLine = 6,
                EndsAtLine = 0x19,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3319",
                ParentId = "3317",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Communication Record",
                StartsAtLine = 6,
                EndsAtLine = 0x17,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3320",
                ParentId = "3317",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Entity Authorization",
                StartsAtLine = 6,
                EndsAtLine = 13,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3321",
                ParentId = "3317",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Beneficiary Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1d,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3322",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Beneficiary Relationship Record",
                StartsAtLine = 6,
                EndsAtLine = 0x18,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3323",
                ParentId = "3322",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Beneficiary Address Record",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3324",
                ParentId = "3322",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Beneficiary Remarks Record",
                StartsAtLine = 6,
                EndsAtLine = 12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3326",
                ParentId = "3322",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1d,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3327",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent Communication Record",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3329",
                ParentId = "3327",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent ID Record",
                StartsAtLine = 6,
                EndsAtLine = 0x16,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3330",
                ParentId = "3327",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent Authorizations Record",
                StartsAtLine = 6,
                EndsAtLine = 13,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3331",
                ParentId = "3327",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Record",
                StartsAtLine = 6,
                EndsAtLine = 0x20,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3332",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Withholding Rec",
                StartsAtLine = 6,
                EndsAtLine = 0x23,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3334",
                ParentId = "3332",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Source Fund",
                StartsAtLine = 6,
                EndsAtLine = 0x12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3335",
                ParentId = "3332",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Payment Details",
                StartsAtLine = 6,
                EndsAtLine = 0x16,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3336",
                ParentId = "3332",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Payout",
                StartsAtLine = 6,
                EndsAtLine = 0x20,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3337",
                ParentId = "3332",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Attachments Record",
                StartsAtLine = 6,
                EndsAtLine = 0x12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3344",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Question Record",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3350",
                ParentId = "3301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Answer Record",
                StartsAtLine = 6,
                EndsAtLine = 14,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3351",
                ParentId = "3350",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        public static DTCCWorkbookGenerationMetadata BuildCOMTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 6),
                new KeyValuePair<string, int>("Description", 7),
                new KeyValuePair<string, int>("ItemNumber", 9),
                new KeyValuePair<string, int>("RequiredIndicator", 10),
                new KeyValuePair<string, int>("RejectCode", 11)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 7,
                EndsAtLine = 0x1f,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 3,
                UniqueId = "C20",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Record",
                StartsAtLine = 7,
                EndsAtLine = 0x1a,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 3,
                UniqueId = "C21",
                ParentId = "C20",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Record #1",
                StartsAtLine = 7,
                EndsAtLine = 0x45,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C2201",
                ParentId = "C21",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Record #2",
                StartsAtLine = 7,
                EndsAtLine = 0x30,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C2202",
                ParentId = "C2201",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Fund Record",
                StartsAtLine = 7,
                EndsAtLine = 0x22,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C2203",
                ParentId = "C2201",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Party Record",
                StartsAtLine = 7,
                EndsAtLine = 0x33,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C2204",
                ParentId = "C2201",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Recipient Record",
                StartsAtLine = 7,
                EndsAtLine = 0x35,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C2205",
                ParentId = "C2201",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        internal static XElement BuildDTCCXml(string fileName)
        {
            int num = 0;
            long num2 = 1L;
            XElement element = new XElement("DTCC");
            element.Add(new XAttribute("XmlUniqueId", num2 += 1L));
            DTCCWorkbookMetadata metadata = null;
            XElement element2 = null;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                num++;
                if (str.StartsWith("HDR.S") || str.StartsWith("1TRANS"))
                {
                    XElement content = new XElement("Datatrak", str);
                    content.Add(new XAttribute("LineNumber", num));
                    content.Add(new XAttribute("XmlUniqueId", num2 += 1L));
                    element.Add(content);
                    metadata = SelectCorrectMetaDataWorkbook(num, lines);
                    if (metadata == null)
                    {
                        throw new InvalidOperationException("Workbook meta data for this file could not be found!");
                    }
                }
                else
                {
                    if (metadata == null)
                    {
                        throw new InvalidOperationException("Workbook meta data for this file could not be found!");
                    }
                    foreach (DTCCSheetMetadata metadata2 in metadata.DTCCSheets)
                    {
                        if ((str.Length <= ((metadata2.TxtFileUniqueIdStartsAt + metadata2.TxtFileUniqueIdLength) - 1)) || !str.Substring(metadata2.TxtFileUniqueIdStartsAt, metadata2.TxtFileUniqueIdLength).Contains(metadata2.UniqueId))
                        {
                            continue;
                        }
                        XElement element4 = new XElement(Utils.RemoveSpecialCharactersForXMLElementNames(metadata2.SheetName));
                        if (string.IsNullOrWhiteSpace(metadata2.ParentId))
                        {
                            element.Add(element4);
                        }
                        else if (element != null)
                        {
                            if ((element2.Attribute("UniqueId") != null) && (element2.Attribute("UniqueId").Value == metadata2.ParentId))
                            {
                                element2.Add(element4);
                            }
                            else
                            {
                                foreach (XElement element5 in element2.Ancestors())
                                {
                                    if ((element5.Attribute("UniqueId") != null) && (element5.Attribute("UniqueId").Value == metadata2.ParentId))
                                    {
                                        element5.Add(element4);
                                        break;
                                    }
                                }
                            }
                        }
                        element2 = element4;
                        element4.Add(new XAttribute("Parent", metadata2.ParentId));
                        element4.Add(new XAttribute("UniqueId", metadata2.UniqueId));
                        element4.Add(new XAttribute("LineNumber", num));
                        element4.Add(new XAttribute("XmlUniqueId", num2 += 1L));
                        foreach (DTCCLineItemMetadata metadata3 in metadata2.DTCCLineItemMetadatas)
                        {
                            XElement element6 = new XElement(Utils.RemoveSpecialCharactersForXMLElementNames(metadata3.FieldName), str.Substring(int.Parse(metadata3.Start) - 1, int.Parse(metadata3.Length)));
                            element6.Add(new XAttribute("Start", metadata3.Start));
                            element6.Add(new XAttribute("End", metadata3.End));
                            element6.Add(new XAttribute("Length", metadata3.Length));
                            element6.Add(new XAttribute("LineNumber", num));
                            element6.Add(new XAttribute("XmlUniqueId", num2 += 1L));
                            element6.Add(new XAttribute("LineItem", true));
                            element4.Add(element6);
                        }
                    }
                }
            }
            Console.WriteLine("There were {0} lines.", num);
            return element;
        }

        public static DTCCWorkbookGenerationMetadata BuildFARTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 6),
                new KeyValuePair<string, int>("Description", 7),
                new KeyValuePair<string, int>("ItemNumber", 9),
                new KeyValuePair<string, int>("RequiredIndicator", 10),
                new KeyValuePair<string, int>("RejectCode", 11)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 7,
                EndsAtLine = 40,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 3,
                UniqueId = "C40",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Header",
                StartsAtLine = 7,
                EndsAtLine = 0x17,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 3,
                UniqueId = "C42",
                ParentId = "C40",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Record",
                StartsAtLine = 7,
                EndsAtLine = 0x3d,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4301",
                ParentId = "C42",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Entity Record",
                StartsAtLine = 7,
                EndsAtLine = 50,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4302",
                ParentId = "C4301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Entity Address Record",
                StartsAtLine = 7,
                EndsAtLine = 0x25,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4303",
                ParentId = "C4302",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Agent Record",
                StartsAtLine = 7,
                EndsAtLine = 0x30,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4304",
                ParentId = "C4301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Transaction Record ",
                StartsAtLine = 7,
                EndsAtLine = 80,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4305",
                ParentId = "C4301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Underlying Assets",
                StartsAtLine = 7,
                EndsAtLine = 0x4d,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4306",
                ParentId = "C4305",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Payee & Payor Record",
                StartsAtLine = 7,
                EndsAtLine = 0x2b,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4307",
                ParentId = "C4305",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Payee & Payor Details",
                StartsAtLine = 7,
                EndsAtLine = 0x2b,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4308",
                ParentId = "C4307",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Payee & Payor Address",
                StartsAtLine = 7,
                EndsAtLine = 0x25,
                TxtFileUniqueIdStartsAt = 0,
                TxtFileUniqueIdLength = 5,
                UniqueId = "C4309",
                ParentId = "C4307",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        private static DTCCWorkbookMetadata BuildFullMetadata(string layoutFilePath, DTCCWorkbookGenerationMetadata dtccWBGenerationMetaData)
        {
            HSSFWorkbook workbook;
            DTCCWorkbookMetadata metadata = new DTCCWorkbookMetadata();
            using (FileStream stream = new FileStream(layoutFilePath, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(stream);
            }
            foreach (DTCCSheetGenerationMetadata sheetGenerationMetaData in dtccWBGenerationMetaData.DTCCSheets)
            {
                DTCCSheetMetadata item = new DTCCSheetMetadata {
                    UniqueId = sheetGenerationMetaData.UniqueId,
                    ParentId = sheetGenerationMetaData.ParentId,
                    SheetName = sheetGenerationMetaData.SheetName,
                    TxtFileUniqueIdStartsAt = sheetGenerationMetaData.TxtFileUniqueIdStartsAt,
                    TxtFileUniqueIdLength = sheetGenerationMetaData.TxtFileUniqueIdLength
                };
                ISheet sheet = workbook.GetSheet(sheetGenerationMetaData.SheetName);
                if (sheet.LastRowNum < sheetGenerationMetaData.EndsAtLine)
                {
                    throw new InvalidOperationException();
                }
                for (int row = sheetGenerationMetaData.StartsAtLine; row < sheetGenerationMetaData.EndsAtLine; row++)
                {
                    if (sheet.GetRow(row) != null)
                    {
                        DTCCLineItemMetadata dtccLineMetaData = new DTCCLineItemMetadata();
                        foreach (var columnMetadata in sheetGenerationMetaData.RelevantColumns)
                        {
                            switch (columnMetadata.Key)
                            {
                                case "Start":
                                    dtccLineMetaData.Start = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "End":
                                    dtccLineMetaData.End = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "Length":
                                    dtccLineMetaData.Length = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "Type":
                                    dtccLineMetaData.Type = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "FieldName":
                                    dtccLineMetaData.FieldName = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "Description":
                                    dtccLineMetaData.Description = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "ItemNumber":
                                    dtccLineMetaData.ItemNumber = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "RequiredIndicator":
                                    dtccLineMetaData.RequiredIndicator = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;

                                case "RejectCode":
                                    dtccLineMetaData.RejectCode = ExcelUtils.GetCellValue(sheet, row - 1, columnMetadata.Value - 1);
                                    break;
                            }                            
                        }
                        if (!string.IsNullOrWhiteSpace(dtccLineMetaData.Start))
                        {
                            item.DTCCLineItemMetadatas.Add(dtccLineMetaData);
                        }
                    }
                }
                metadata.DTCCSheets.Add(item);
            }
            return metadata;
        }

        public static DTCCWorkbookGenerationMetadata BuildPFFTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 6),
                new KeyValuePair<string, int>("Description", 7),
                new KeyValuePair<string, int>("ItemNumber", 9),
                new KeyValuePair<string, int>("RequiredIndicator", 10),
                new KeyValuePair<string, int>("RejectCode", 11)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 7,
                EndsAtLine = 0x21,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "10",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Record",
                StartsAtLine = 7,
                EndsAtLine = 0x1c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "12",
                ParentId = "10",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Record",
                StartsAtLine = 7,
                EndsAtLine = 0x42,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1301",
                ParentId = "12",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Valuation Record",
                StartsAtLine = 7,
                EndsAtLine = 0x41,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1302",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Underlying Assets",
                StartsAtLine = 7,
                EndsAtLine = 0x37,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1303",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Band Guaranteed Loop",
                StartsAtLine = 9,
                EndsAtLine = 60,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1304",
                ParentId = "1303",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Events Record",
                StartsAtLine = 7,
                EndsAtLine = 0x4c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1307",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        public static DTCCWorkbookGenerationMetadata BuildPNFTopLevelMetadata() => 
            BuildPVFTopLevelMetadata();

        public static DTCCWorkbookGenerationMetadata BuildPVFTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 6),
                new KeyValuePair<string, int>("Description", 7),
                new KeyValuePair<string, int>("ItemNumber", 9),
                new KeyValuePair<string, int>("RequiredIndicator", 10),
                new KeyValuePair<string, int>("RejectCode", 11)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 7,
                EndsAtLine = 0x21,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "10",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Record",
                StartsAtLine = 7,
                EndsAtLine = 0x1c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "12",
                ParentId = "10",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Record",
                StartsAtLine = 7,
                EndsAtLine = 0x42,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1301",
                ParentId = "12",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Valuation Record",
                StartsAtLine = 7,
                EndsAtLine = 0x41,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1302",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Underlying Assets",
                StartsAtLine = 7,
                EndsAtLine = 0x37,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1303",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Band Guaranteed Loop",
                StartsAtLine = 9,
                EndsAtLine = 60,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1304",
                ParentId = "1303",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Agent Record ",
                StartsAtLine = 7,
                EndsAtLine = 0x2e,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1305",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Dates Record ",
                StartsAtLine = 7,
                EndsAtLine = 0x88,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1306",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Events Record",
                StartsAtLine = 7,
                EndsAtLine = 0x4c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1307",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Party Record",
                StartsAtLine = 7,
                EndsAtLine = 0x3f,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1309",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Address Record",
                StartsAtLine = 7,
                EndsAtLine = 40,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1310",
                ParentId = "1309",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Annuitization Record",
                StartsAtLine = 7,
                EndsAtLine = 0x4c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1311",
                ParentId = "1309",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Communication Record",
                StartsAtLine = 6,
                EndsAtLine = 0x19,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1312",
                ParentId = "1309",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Service Feature Record",
                StartsAtLine = 7,
                EndsAtLine = 0x4c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "1315",
                ParentId = "1301",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        public static DTCCWorkbookGenerationMetadata BuildSUBTopLevelMetadata()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>> {
                new KeyValuePair<string, int>("Start", 1),
                new KeyValuePair<string, int>("End", 2),
                new KeyValuePair<string, int>("Length", 3),
                new KeyValuePair<string, int>("Type", 4),
                new KeyValuePair<string, int>("FieldName", 5),
                new KeyValuePair<string, int>("Description", 6),
                new KeyValuePair<string, int>("ItemNumber", 7),
                new KeyValuePair<string, int>("RequiredIndicator", 8),
                new KeyValuePair<string, int>("RejectCode", 9)
            };
            DTCCSheetGenerationMetadata item = new DTCCSheetGenerationMetadata {
                SheetName = "Submitting Header ",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "30",
                ParentId = string.Empty,
                RelevantColumns = list
            };
            DTCCWorkbookGenerationMetadata metadata1 = new DTCCWorkbookGenerationMetadata();
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contra Header",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 2,
                UniqueId = "32",
                ParentId = "30",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application-Sub-Pay Record",
                StartsAtLine = 7,
                EndsAtLine = 0x2c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3501",
                ParentId = "32",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application Date Record",
                StartsAtLine = 7,
                EndsAtLine = 70,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3502",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Application Contract Record ",
                StartsAtLine = 7,
                EndsAtLine = 0x17,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3503",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Method",
                StartsAtLine = 6,
                EndsAtLine = 0x1b,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3504",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Type Record",
                StartsAtLine = 6,
                EndsAtLine = 0x10,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3506",
                ParentId = "3504",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Cost Basis Record",
                StartsAtLine = 6,
                EndsAtLine = 13,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3507",
                ParentId = "3506",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Funds Record",
                StartsAtLine = 6,
                EndsAtLine = 0x15,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3510",
                ParentId = "3506",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Qualified Transfer Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1b,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3513",
                ParentId = "3506",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Qual Trans Prior Carr Add",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3514",
                ParentId = "3513",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "1035 Transfer Prior Con Entity",
                StartsAtLine = 7,
                EndsAtLine = 0x1c,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3515",
                ParentId = "3513",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Premium Type Remarks Record",
                StartsAtLine = 6,
                EndsAtLine = 12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3516",
                ParentId = "3506",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Contract Entity ID Record",
                StartsAtLine = 6,
                EndsAtLine = 0x24,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3517",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent Record",
                StartsAtLine = 6,
                EndsAtLine = 0x1d,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3527",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Agent ID Record",
                StartsAtLine = 6,
                EndsAtLine = 0x16,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3530",
                ParentId = "3527",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Record",
                StartsAtLine = 6,
                EndsAtLine = 0x20,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3532",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Service Feature Source Fund",
                StartsAtLine = 6,
                EndsAtLine = 0x12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3535",
                ParentId = "3532",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Attachments Record",
                StartsAtLine = 6,
                EndsAtLine = 0x12,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3544",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Question Record",
                StartsAtLine = 6,
                EndsAtLine = 0x11,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3550",
                ParentId = "3501",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            item = new DTCCSheetGenerationMetadata {
                SheetName = "Answer Record",
                StartsAtLine = 6,
                EndsAtLine = 14,
                TxtFileUniqueIdStartsAt = 1,
                TxtFileUniqueIdLength = 4,
                UniqueId = "3551",
                ParentId = "3550",
                RelevantColumns = list
            };
            metadata1.DTCCSheets.Add(item);
            return metadata1;
        }

        private static string FormatDTCCSupportedFormatsDictionaryKeys(DTCCFormatMetadata dtccFormatMetadata) => 
            $"{dtccFormatMetadata.DTCCDataTrakServiceNameString}*{dtccFormatMetadata.DTCCIPSBusinessCode}*{dtccFormatMetadata.DTCCIPSEventCode}";

        public static Dictionary<string, DTCCFormatMetadata> InitializeSupportedFormats()
        {
            DTCCFormatMetadata metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\FAR.xls"
            };
            DTCCWorkbookGenerationMetadata dtccWBGenerationMetaData = BuildFARTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "FINANCIAL ACTIVITY";
            metadata.DTCCIPSBusinessCode = "FAR";
            metadata.DTCCIPSEventCode = string.Empty;
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\POV.xls"
            };
            dtccWBGenerationMetaData = BuildPVFTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "POSITIONS & VALUES";
            metadata.DTCCIPSBusinessCode = "POV";
            metadata.DTCCIPSEventCode = "PVF";
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\POV.xls"
            };
            dtccWBGenerationMetaData = BuildPNFTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "POSITIONS & VALUES";
            metadata.DTCCIPSBusinessCode = "POV";
            metadata.DTCCIPSEventCode = "PNF";
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\POV.xls"
            };
            dtccWBGenerationMetaData = BuildPFFTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "POSITIONS & VALUES";
            metadata.DTCCIPSBusinessCode = "POV";
            metadata.DTCCIPSEventCode = "PFF";
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\APPSUB.xls"
            };
            dtccWBGenerationMetaData = BuildAPPTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "APP/SUB";
            metadata.DTCCIPSBusinessCode = "PRM";
            metadata.DTCCIPSEventCode = "APP";
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\APPSUB.xls"
            };
            dtccWBGenerationMetaData = BuildSUBTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "APP/SUB";
            metadata.DTCCIPSBusinessCode = "PRM";
            metadata.DTCCIPSEventCode = "SUB";
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata {
                DTCCLayoutFilePath = @"DTCCLayouts\COM.xls"
            };
            dtccWBGenerationMetaData = BuildCOMTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "COMMISSIONS";
            metadata.DTCCIPSBusinessCode = "COM";
            metadata.DTCCIPSEventCode = string.Empty;
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);

            metadata = new DTCCFormatMetadata
            {
                DTCCLayoutFilePath = @"DTCCLayouts\DATATRAK.xls"
            };
            dtccWBGenerationMetaData = BuildDATATRAKTopLevelMetadata();
            metadata.DTCCCompleteWorkbookMetadata = BuildFullMetadata(metadata.DTCCLayoutFilePath, dtccWBGenerationMetaData);
            metadata.DTCCDataTrakServiceNameString = "HDR";
            metadata.DTCCIPSBusinessCode = "HDR";
            metadata.DTCCIPSEventCode = string.Empty;
            supportedDTCCFormats.Add(FormatDTCCSupportedFormatsDictionaryKeys(metadata), metadata);
            return supportedDTCCFormats;
        }

        private static DTCCWorkbookMetadata SelectCorrectMetaDataWorkbook(int txtFileLineCounter, IEnumerable<string> lines)
        {
            string str = lines.ElementAt<string>(txtFileLineCounter - 1);
            foreach (KeyValuePair<string, DTCCFormatMetadata> pair in supportedDTCCFormats)
            {
                char[] separator = new char[] { '*' };
                string[] strArray = pair.Key.Split(separator);
                if (str.Contains(strArray[0]))
                {
                    string str2 = lines.ElementAt<string>(txtFileLineCounter + 1);
                    if (lines.ElementAt<string>(txtFileLineCounter).Contains(pair.Value.DTCCIPSBusinessCode))
                    {
                        if (string.IsNullOrWhiteSpace(pair.Value.DTCCIPSEventCode))
                        {
                            return pair.Value.DTCCCompleteWorkbookMetadata;
                        }
                        if (str2.Contains(pair.Value.DTCCIPSEventCode))
                        {
                            return pair.Value.DTCCCompleteWorkbookMetadata;
                        }
                    }
                }
            }
            return null;
        }
    }
}

