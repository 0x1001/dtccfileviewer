﻿namespace DTCCFileViewer
{
    using System;
    using System.Collections.Generic;

    public class DTCCWorkbookMetadata
    {
        public List<DTCCSheetMetadata> DTCCSheets = new List<DTCCSheetMetadata>();
    }
}

