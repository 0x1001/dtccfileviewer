﻿namespace DTCCFileViewer
{
    using ICSharpCode.AvalonEdit.Document;
    using ICSharpCode.AvalonEdit.Rendering;
    using System;
    using System.Windows;
    using System.Windows.Media;

    public class LineColorizer : DocumentColorizingTransformer
    {
        private int endOffset;
        private int lineNumber;
        private int startOffset;

        public LineColorizer(int lineNumber, int startOffset, int endOffset)
        {
            if (lineNumber < 1)
            {
                throw new ArgumentOutOfRangeException("lineNumber", lineNumber, "Line numbers are 1-based.");
            }
            this.lineNumber = lineNumber;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        private void ApplyChanges(VisualLineElement element)
        {
            element.TextRunProperties.SetForegroundBrush(Brushes.Red);
            element.TextRunProperties.SetBackgroundBrush(Brushes.Yellow);
            Typeface typeface = element.TextRunProperties.Typeface;
            element.TextRunProperties.SetTypeface(new Typeface(typeface.FontFamily, FontStyles.Normal, FontWeights.SemiBold, typeface.Stretch));
        }

        protected override void ColorizeLine(DocumentLine line)
        {
            if ((line.Length != 0) && (!line.IsDeleted && (line.LineNumber == this.lineNumber)))
            {
                base.ChangeLinePart((line.Offset + this.startOffset) - 1, ((line.Offset + this.startOffset) + this.endOffset) - 1, new Action<VisualLineElement>(this.ApplyChanges));
            }
        }

        public int LineNumber
        {
            get => 
                this.lineNumber;
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value", value, "Line numbers are 1-based.");
                }
                this.lineNumber = value;
            }
        }
    }
}

