﻿namespace DTCCFileViewer
{
    using System;
    using System.Runtime.CompilerServices;

    public class DTCCFormatMetadata
    {
        public DTCCWorkbookMetadata DTCCCompleteWorkbookMetadata { get; set; }

        public string DTCCDataTrakServiceNameString { get; set; }

        public string DTCCIPSBusinessCode { get; set; }

        public string DTCCIPSEventCode { get; set; }

        public string DTCCLayoutFilePath { get; set; }

        public string DTCCServiceEventCode { get; set; }
    }
}

